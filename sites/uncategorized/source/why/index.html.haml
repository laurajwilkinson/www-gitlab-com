---
title: Why GitLab?
description: "GitLab shortens your DevOps cycle time, bridges silos and stages, and takes work out of your hands. Learn more here!"
canonical_path: "/why/"
suppress_header: true
extra_css:
  - auto-devops.css
  - cta-promo.css
  - case-study.css
extra_js:
  - features.js
---

.blank-header
  = image_tag "/images/home/icons-pattern-left.svg", class: "image-border image-border-left", alt: "Gitlab hero border pattern left svg"
  = image_tag "/images/home/icons-pattern-right.svg", class: "image-border image-border-right", alt: "Gitlab hero border pattern right svg"
  .flex-container.flex-column.justify-center.align-center
    %h1 Why GitLab?
    %p We simplify software development so you can focus on what matters most—your customers.
    = link_to "Start a trial", "/free-trial/", class: "btn cta-btn accent"

.content-container
  .content.tile
    :markdown
      ## Simplified DevOps

      GitLab is a complete DevOps platform that brings development, operations, and security teams into a single application. GitLab helps teams accelerate software delivery from weeks to minutes while reducing development costs and security risks.

  .content.grid-layout.grid-layout3
    .tile.tile-animated.tile-compact.tile-left-aligned.grid-item
      .tile-content
        %h4.text-center Dev and Ops teams working together
        %ul 
          %li Everyone working in the same system
          %li Shared feedback loops
          %li No messy integrations
    .tile.tile-animated.tile-compact.tile-left-aligned.grid-item
      .tile-content
        %h4.text-center Get products to users faster
        %ul 
          %li Issues addressed earlier
          %li Fast cycle times
          %li Quality control
    .tile.tile-animated.tile-compact.tile-left-aligned.grid-item
      .tile-content
        %h4.text-center Reduce security and compliance risk
        %ul 
          %li Every change is fully tested and secure
          %li Audit logs for every action
          %li Single sign-on and datastore

  .content.tile
    %h2.text-center BI Worldwide increases deployments to 10 times per day
    .customer-case-item
      %blockquote
        .quote-body
          “One tool for SCM+CI/CD was a big initial win. Now wrapping security scans into that tool as well has already increased our visibility into security vulnerabilities. The integrated Docker registry has also been very helpful for us. Issue/Product management features let everyone operate in the same space regardless of role.”
        .quote-attribution Adam Dehnel
        .quote-attribution-title Product Architect, BI WORLDWIDE
        %br/
        %br/
        %a.text-right{ "href": "/customers/bi_worldwide/" }
          %strong Read the story 

    = partial "includes/home/customer-logos-transparent"

  .content.tile
    :markdown
      ## Bridging Silos and Stages

      In GitLab, everyone looks at the same things. There is a single source of truth for every single change, and it is linked automatically to anything relevant. This is because GitLab is a single application. Project management, code review, continuous integration and delivery – and even application security and monitoring are all part of GitLab. This means:

      - No more formal handoffs between teams
      - No need to maintain and secure integration points
      - There is always a clear single source of truth

      We like to think of the DevOps lifecycle becoming a single conversation, where GitLab automatically links
      all relevant information together.

      ![](/images/why/thread2.png)

      And this means one team no longer needs to wait for the handoff of another team. For example:

      - QA can join the conversation earlier in the cycle, potentially catching issues early on and
      reducing bottlenecks and gatekeeping later in the lifecycle.
      - The developer can ping the security pro in a comment to ask for help with vulnerable code with both looking at the same scan results.


      We call this [Concurrent DevOps](/topics/concurrent-devops/).

      ![](/images/why/gatek.png)

      [See more advantages of GitLab being a single application](/handbook/product/single-application/)

  .content.tile
    :markdown
      ## Benefits of shorter cycle time

      A shorter cycle allows you to skate to where the puck is going to be. That means it takes
      less time to go from an idea for a change to actually having that change live in a production
      environment, monitored and ready for scaling.

      ![](https://docs.gitlab.com/ee/img/devops-stages.png)

      By shortening this time, you're able to respond to changing needs from the market faster,
      adjust your long-term plans with feedback you receive on the way, and radically reduce
      engineering risk.

      ### Shorter feedback cycle

      ![](/images/why/puck.png)

      A shorter cycle time allows you to immediately respond to changing needs. Rather than having
      to wait upwards of several months for the DevOps cycle to complete, with a short cycle you're
      able to adjust your plans quickly. With each iteration you complete, you get new opportunities
      to collect feedback. Be that low-level feedback on the performance of your products, or direct
      feedback from your customers. GitLab has monitoring built-in, so no time is wasted on trying
      to get information surfaced.


      ### Reduce engineering risk

      In addition to creating a shorter feedback cycle and being able to respond quicker to changing needs,
      a surprising advantage of shorter cycles is reduced engineering risk. Shorter cycles mean more
      and smaller frequent deploys, which have many advantages:

      - Easier to coordinate and reason about
      - Higher predictability: smaller iterations are easier to estimate than larger ones
      - Better code quality: every small change gets attention, rather than all at once
      - Easier troubleshooting: a smaller deploy introduces less changes that can potentially introduce issues

      ### More secure applications

      Shorter cycle times means feedback on security vulnerabilities is delivered directly to the responsible developer right in their merge request pipeline report. Even dynamic application security testing (DAST), that requires a working application, can be done prior to merging the branch, using GitLab's review app function. Many vulnerabilities can be removed during development without wasting cycles downstream for security teams to vet findings, prioritize and triage and then create tickets to remediate.


      GitLab shortens your cycles by bringing everyone together and taking work out of your hands.

      [Read about Cycle Analytics](/stages-devops-lifecycle/value-stream-analytics/)

  .content
    = partial "includes/ctas/cta-promo"
