---
layout: markdown_page
title: "Cloud Native for Business"
description: "Cloud Native applications use containers, microservices architecture, and container orchestration like Kubernetes. Learn more here!"
canonical_path: "/solutions/cloud-native/"
---
## GitLab is the easiest way to build Cloud Native Applications

[Cloud native](/cloud-native/) applications use containers, 
microservices architecture, and container orchestration like
Kubernetes. GitLab is designed for cloud native applications with 
tight [Kubernetes integration](/solutions/kubernetes/). 

Businesses are shifting from traditional deployment models to cloud 
native applications in order to gain speed, reliability and scale. 

Learn more about how GitLab can power your cloud native development.

[![What You Need to Know About Going Cloud Native](https://img.youtube.com/vi/wtaOQY_ITvQ/0.jpg)](http://www.youtube.com/watch?v=wtaOQY_ITvQ "What You Need to Know About Going Cloud Native")
