---
layout: handbook-page-toc
title: "How to evaluate a remote job"
canonical_path: "/company/culture/all-remote/evaluate/"
description: How to evaluate a remote job
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

On this page, we're detailing how to evaluate a remote job.

## Not every remote job is created equal

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/4bO-wxLQtJQ" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) video above, Darren (GitLab) and Rodolphe (Remotive) discuss several remote work topics: favorite things about remote work, how to find/start/thrive in your first remote role, and the impact of the growing remote work community.* 

While [job sites](/company/culture/all-remote/jobs/) and common conversation can lump each remote role into a single category, it's important to realize that not every remote role is created equal. Just as colocated roles need to be evaluated in totality, those seeking to transition into a remote role for the first time should ask certain questions during the interview or evaluation phase. 

There are [various stages of remote work](/company/culture/all-remote/stages/), which can be easily pictured as a sliding scale. From no remote to [hybrid-remote](/company/culture/all-remote/hybrid-remote/) to all-remote, and all points in between, a remote worker's experience in each setting will likely vary. Below, we're highlighting key topics to cover when seeking an understanding of how you'll be treated in a full-time remote role. Additional questions should be considered depending on your specific scenario and familiarity with the hiring company. 

While there are a growing number of freelance gigs which can be completed remotely, this article is centered around [full-time remote careers](/company/culture/all-remote/jobs/).

## Where does leadership work?

![GitLab transport illustration](/images/all-remote/gitlab-transport.jpg){: .medium.center}

When evaluating a remote role, it's critical to understand where leadership works on a day-to-day basis. In an all-remote company, each team member — including the executive team — is remote. This creates a culture of trust and [transparency](/handbook/values/#transparency), and it sends a clear message that no one will be treated as an outsider within the organization based on their geographic location. 

In companies where the leadership team is colocated, but most (if not all) of the company is remote, consider asking how this impacts the dissemination of information. Remote [forces companies to do things they ought to be doing earlier and better](/company/culture/all-remote/management/#how-do-you-manage-a-100-remote-team), including [documenting](/handbook/documentation/) culture and process. With a colocated leadership team, a company runs the risk of bypassing diligent documentation in favor of an in-person sync where discussion notes and outcomes are not documented and shared.

## Are hybrid calls utilized or tolerated? 

![GitLab collaboration illustration](/images/all-remote/gitlab-collaboration-illustration.jpg){: .medium.center}

In [hybrid-remote](/company/culture/all-remote/hybrid-remote/) companies, where certain team members are colocated and others are remote, consider asking how [meetings](/company/culture/all-remote/meetings/) and calls are handled. [Hybrid calls](/handbook/communication/#hybrid-calls-are-horrible) are often allowed or encouraged, though they offer a suboptimal experience for remote attendees. Ideally, each call partipant will have their own equipment (camera, headset, and screen), even if multiple members are colocated.

This ensures that each team member is treated equally, and removes typical boardroom issues such as difficulty seeing body language through a wide-angle camera and being interrupted or booted from a room. 

## What percentage of your group meetings last week resulted in a written artifact?

This question dives into a couple of areas. It provides an opportunity to understand if a company truly embraces asynchronous communication, and it exposes their true view and use of [meetings](/company/culture/all-remote/meetings/). 

## Is there flexibility with working hours and days?

![GitLab collaboration illustration](/images/all-remote/gitlab-collaboration.jpg){: .medium.center}

Flex-work and workplace flexibility have grown in popularity, but it's important to ask for specific examples of how this actually works within a company you are considering joining. 

Do only senior leaders have complete flexibility to schedule their working hours and days around their lives? Is workplace flexibility truly embraced and supported throughout the organization? Does the company empower team members to be a [manager of one](/handbook/values/#managers-of-one), valuing [outputs](/handbook/values/#results) rather than inputs such as hours worked? 

When life happens and you need to restructure your day on the fly, is this openly encouraged and supported or quietly frowned upon? 

Answers to these questions are critical in understanding the [true level of autonomy](/company/culture/all-remote/people/#those-who-value-flexibility-and-autonomy) you'll be given to add value in a given company. 

## Do teams default to asynchronous, or adhere to select time zones? 

![GitLab journey illustration](/images/all-remote/gitlab-journey-and-navigation.jpg){: .medium.center}

It's important to understand how teams work within an all-remote company. In organizations where [asynchronous communication](/handbook/communication/) is the default, it's likely that team members are diligent about documentation, resist the urge to schedule [meetings](/company/culture/all-remote/meetings/), and are sensitive to atypical working hours/arrangements. 

Certain companies prefer to have everyone contributing between certain times, creating additional boundaries on those who live too far east/west from the proverbial company epicenter and for [nomads](/company/culture/all-remote/people/#nomads) who regularly change locations and time zones. 

## How is information disseminated?

![GitLab code illustration](/images/all-remote/gitlab-code-review.jpg){: .medium.center}

[Documentation](/handbook/documentation/) is important in all companies, and the answer to this question should shed light on how much a company values it. Ideally, a company will have a handbook or similar where processes and culture are documented first, and *then* shared out (via email, Slack, etc.). 

## Approximately how many times did you reference the company handbook last month, and propose changes to it?

![GitLab values illustration](/images/all-remote/gitlab-values-tanukis.jpg){: .medium.center}

Similar to the above, this question allows you to talk specifics on how processes and solutions are iterated upon, and who is empowered to contribute. (*Hat tip to [Wes Winham Winler](https://twitter.com/weswinham/status/1212044671091978240) for the question*.)

## Are cloud collaboration tools the default?

![GitLab review illustration](/images/all-remote/gitlab-review-and-collaborate.jpg){: .medium.center}

Consider asking if notes are captured using an online collaborative suite (Office 365, Google G Suite, etc.). This is a more transparent, efficient, and collaborative approach compared to emailing documents back and forth, which creates versioning headaches. 

## How is informal communication encouraged and enabled?

![GitLab collaboration illustration](/images/all-remote/gitlab-collaboration.jpg){: .medium.center}

Remote companies must be intentional about [informal communication](/company/culture/all-remote/informal-communication/). When some or all team members are not able to foster relationships in person, a company must take deliberate steps. Consider asking for specific [examples](/handbook/incentives/#visiting-grant) of how a company has created an atmopshere where team members are [encouraged to connect as humans](/blog/2017/06/30/there-and-back-again-in-one-release/), not just colleagues. 

## What percentage of your team was hired without an in-person meeting?

![GitLab commit illustration](/images/all-remote/gitlab-commit-illustration.jpg){: .medium.center}

Breaking the habit of requiring an in-person touch point for situations of significance (e.g. hiring someone, bringing on a new agency partner, etc.) is tough. While there is nothing inherently wrong with having an in-person touch point along the hiring process if it's convenient, you'll learn a lot about an organization's faith in [virtual conversations](/blog/2019/08/05/tips-for-mastering-video-calls/) by asking this question. 

## What percentage of your teams social/team-building activities last quarter were remote (versus in-person)?

[In-person interactions](/company/culture/all-remote/in-person/) are important in remote organizations. Asking this question provides valuable insight into a company's strategy for building bonds through planned in-person activities, as well as how equipped they are to faciliate team-building through remote interactions. 

## <%= partial("company/culture/all-remote/is_this_advice_any_good_remote.erb") %>

## Contribute your lessons

GitLab believes that all-remote is the [future of work](/company/culture/all-remote/vision/), and remote companies have a shared responsibility to show the way for other organizations who are embracing it. If you or your company has an experience that would benefit the greater world, consider creating a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/) and adding a contribution to this page.

----

Return to the main [all-remote page](/company/culture/all-remote/).
