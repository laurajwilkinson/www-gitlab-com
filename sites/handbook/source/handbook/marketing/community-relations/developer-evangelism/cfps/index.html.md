---
layout: handbook-page-toc
title: "Developer Evangelism CFPs"
---

## How we manage CFPs (Call for Proposals)

### Our events list
Every year, developer evangelism prioritizes some key events in our ecosystem for which we run the conference proposal (CFP) process. These events are maintained in the [Events Master List sheet](https://docs.google.com/spreadsheets/d/1KX8uf-4Ov8ybztJibQlGr9HvgH9VobpA8Nv5ecny1N4/edit#gid=1294176754) as we add suggestions that fulfill our requirements for focus events. You can also find a calendar view of our events below:

<br>

<iframe src="https://calendar.google.com/calendar/embed?src=gitlab.com_eta7o4tn4btn8h0f8eid5q98ro%40group.calendar.google.com&ctz=Europe%2FAmsterdam" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>

### CFP Library 

Past submissions for CFPs are stored in the [CFP Library](https://docs.google.com/spreadsheets/d/1KX8uf-4Ov8ybztJibQlGr9HvgH9VobpA8Nv5ecny1N4/edit#gid=0).

## Requesting a Developer Evangelist to submit a CFP
To request that a Developer Evangelist submits a CFP to your event, please:

1. [Open a new issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=developer-evangelist-request) 
1. Ensure you use the `developer-evangelist-request` template, and fill out the `External Request` section.
1. Label the issue with the `CFP` label.

### CFP Management
For every CFP process we are participating in, a [CFP issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=CFP-Meta) is created with details about the event, requirements for CFP submissions and any other relevant information that might be useful to potential speakers. The issue template contains guide on the necessary steps needed to ensure we keep track of all the submissions for each CFP.

Once a submission has been submitted, the author should add it to the [CFP Library](https://docs.google.com/spreadsheets/d/1KX8uf-4Ov8ybztJibQlGr9HvgH9VobpA8Nv5ecny1N4/edit#gid=0). This will allow us to repurpose CFPs efficiently.

#### Issue Workflow

We monitor 2 GitLab issue types for CFPs:

1. A meta issue for the call for proposals
1. Submission issues created by interested speakers that relate to the meta issue

All CFP issues have the `CFP` label. These issues appear on the dedicated [CFP Issue board](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/boards/2415569?&label_name[]=CFP).

Afterward, our workflow uses scoped labels to transition the issues through different stages.

| **CFP Labels** |  **Description** |
| ------ |  ------ |
| `CFP::Open` |  Identifies a CFP meta issue that is still open for submissions |
| `CFP::Closed` |  Identifies a CFP meta issue that already has passed due date |
| `CFP::Cancelled` |  Identifies a CFP meta issue for an event that has been cancelled |
| `CFP-Upcoming` |   This labels is used to track CFPs that are near due dates. | 
| `CFP::Draft` |  Identifies CFP (Call for Proposal) Drafts that are under Developer Evangelism Team Radar | 
| `CFP::Review` |  Identifies CFP (Call for Proposal) Drafts that require review by the Developer Evangelism Team | 
| `CFP::Submitted` |  Identifies submitted CFPs (Call for Proposal) and monitored by the Developer Evangelism team |
| `CFP::Rejected` |   Identifies Rejected CFPs (Call for Proposal) and monitored by the Developer Evangelism team |
| `CFP::Accepted` |  Identifies Accepted CFPs (Call for Proposal) and monitored by the Developer Evangelism team | 
| `CFP::Missed` |  Identifies CFPs (Call for Proposal) that missed submission | 


For a CFP submission the transition is depicted below:

```plantuml
start
: CFP, CFP::Draft;
: CFP, CFP::Review;
: CFP, CFP::Submitted;
if (CFP is Accepted) then (yes)
    : CFP, CFP::Accepted;
elseif (CFP is Rejected) then (yes)
    : CFP, CFP::Rejected;
elseif (CFP is Waitlisted) then (yes)
    : CFP, CFP::Waitlist;
else  (nothing)
    : CFP, CFP::missed;
endif
stop

```
