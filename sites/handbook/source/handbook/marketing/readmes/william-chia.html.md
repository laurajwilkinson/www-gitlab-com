---
layout: markdown_page
title: "William Chia's README"
description: "A personal README for William Chia. Get to know me a bit better." 
---

## William Chia README

Sr. Product Marketing Manager, Cloud Native & GitOps / Product Management Intern Monitor:Health

## Related pages

* [linkedin.com/in/williamchia/](https://www.linkedin.com/in/williamchia/)
* [twitter.com/thewilliamchia](https://twitter.com/thewilliamchia)
* [gitlab.com/williamchia](https://gitlab.com/williamchia)

## About me

* I am a big fan of the Iteration value and [Minimum Viable Change](/handbook/values/#minimal-viable-change-mvc)
* I prefer asynchronous comunication - issues, slack, email, etc. 
* If you ping me on a GitLab issue and don't get a response I appreciate a ping on slack. 
* I like meetings for discussion and getting to know you better. 
* I get frustrated with meetings whose purpose is to report progress (this is usually better done in an asyn format.)
* I am extremely honest and transparent. 
    * If I say I like something I mean it.
    * If I say I don't like somethign I mean it. 
    * If I say I don't have a strong opinion I mean it.
* A downside to my transparency is I'll tell you exactly what I'm thinking even if it's half-baked or not thought out at all. It can sometimes be frustrating for people when later I change my mind because I thought about it more or learned new information. I try to be clear when I'm sharing a top-of-mind gut feeling vs somthing I'm more confident about. It's ok to ask me to clarify which. 
* I tend to be an oral processor (I think by talking). It's easier for me to figure things out by having a conversation about it than it is for me to go off on my own and think about it. 
* When I get stressed I want to engage with people more. Some people need a break and alone time when the situation gets stressful. It's ok to tell me, "let's take a break." and I'll respect that. 
* I am often my sharpest late at night. If you see commits, comments, slacks, etc. coming in the middle of the night it doesn't mean I work non-stop. It probably means I took a nap that afternoon. Async all-remote FTW! 
* I'm not a morning person. I apologize in advance for the lack of cohesion my thoughts may carry on my first call or two of the day. 

## Books 

These are books that have made an impact on me and I often recommend and make reference to

* [Made to Stick](https://heathbrothers.com/books/made-to-stick/)
* [Concious Business](https://www.amazon.com/Conscious-Business-Build-through-Values/dp/1622032020)
